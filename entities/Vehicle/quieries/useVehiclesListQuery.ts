import {useQuery} from "@tanstack/react-query";
import {MockApiClient} from "../../../shared/lib/MockApiClient/MockApiClient";
import {Vehicle} from "../Vehicle";

const VEHICLES_LIST_QUERY_KEY = "vehicles_list";

const vehiclesListQuery = () => MockApiClient('/vehicles') as Promise<Vehicle[]>

export const useVehiclesListQuery = () => useQuery({
    queryKey: [VEHICLES_LIST_QUERY_KEY],
    queryFn: vehiclesListQuery,
    initialData: [],
});
