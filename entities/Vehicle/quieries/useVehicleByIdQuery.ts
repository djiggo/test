import {useQuery} from "@tanstack/react-query";
import {MockApiClient} from "../../../shared/lib/MockApiClient/MockApiClient";
import {Vehicle} from "../Vehicle";

const VEHICLE_QUERY_KEY = "vehicle";

const vehicleByIdQuery = (id: Vehicle['id']) => MockApiClient(`/vehicle/${id}`) as Promise<Vehicle>

export const useVehicleByIdQuery = (id: Vehicle['id']) => useQuery({
    queryKey: [VEHICLE_QUERY_KEY, id],
    queryFn: () => vehicleByIdQuery(id),
});
