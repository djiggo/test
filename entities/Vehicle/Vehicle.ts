import {Driver} from "../Driver/Driver";

export enum VehicleType {
    Cargo = 'cargo',
    Passenger = 'passenger',
    Special = 'special',
}

export type Vehicle = {
    id: number,
    type: VehicleType,
    driver: Driver,
    position: {
        latitude: number,
        longitude: number
    }
}