import {View} from "react-native";
import {VehiclesList} from "../../widgets/VehiclesList/VehiclesList";

export default function VehiclesPage() {

    return <View>
        <VehiclesList/>
    </View>


}
