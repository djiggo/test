import {Stack, Tabs} from "expo-router";
import React from "react";
import {Ionicons} from '@expo/vector-icons';
import {View} from "react-native";

export default function Layout() {
    return <Tabs>
        <Tabs.Screen
            name="index"
            options={{
                title: "List",
                tabBarIcon: ({color, size}) => <Ionicons name="list" color={color} size={size}/>
            }}
        />
        <Tabs.Screen
            name="map" options={{
            title: "Map",
            tabBarIcon: ({color, size}) => <Ionicons name="map" color={color} size={size}/>
        }}
        />
        <Tabs.Screen name="[vehicleId]" options={{href: null}}/>
    </Tabs>
}

