import {View} from "react-native";
import {useVehiclesListQuery} from "../../entities/Vehicle/quieries/useVehiclesListQuery";
import {VehiclesMap} from "../../widgets/VehiclesMap/VehiclesMap";

export default function VehiclesMapPage() {

    return <View>
        <VehiclesMap/>
    </View>


}
