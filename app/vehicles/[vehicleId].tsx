import {View} from "react-native";
import {Tabs, useLocalSearchParams} from "expo-router";
import {useVehicleByIdQuery} from "../../entities/Vehicle/quieries/useVehicleByIdQuery";
import {getMarkersBoundingBox} from "../../shared/lib/geo/getMarkersBoundingBox";
import MapView, {Marker} from "react-native-maps";
import {Box, VStack, Text, HStack, Spacer, Pressable, Button} from "native-base";
import {Linking} from 'react-native'

export default function VehiclesPage() {

    const {vehicleId} = useLocalSearchParams();

    const {data: vehicle, isLoading} = useVehicleByIdQuery(+vehicleId);

    if (!vehicle || isLoading) {
        return <View/>;
    }

    const call = () => {
        Linking.openURL(`tel:${vehicle.driver.phone}`);
    }
    const write = () => {
        const text = 'Добрый день, подскажите пожалуйста, какой номер заказа у вас сейчас в работе';

        Linking.openURL(`whatsapp://send?text=${encodeURIComponent(text)}&phone=${vehicle.driver.phone}`)
    }
    return <View style={{flex: 1}}>
        <Tabs.Screen options={{title: `ТС #${vehicle.id.toString()}`}}/>

        <MapView
            style={{width: "100%", flexGrow: 1}}
            initialRegion={getMarkersBoundingBox([vehicle.position], 0.02)}
        >
            <Marker
                key={vehicle.id}
                coordinate={vehicle.position}
                title={`ТС #${vehicle.id}`}
            />
        </MapView>

        <Box padding={[3]}>
            <Text>Type: {vehicle.type}</Text>
        </Box>


        <Box padding={[3]}>
            <Text>Driver name: {vehicle.driver.name}</Text>
        </Box>
        <Box padding={[3]}>
            <Text>Driver phone: {vehicle.driver.phone}</Text>
        </Box>
        <HStack space={[2, 3]}>
            <Button style={{flex: 1}} onPress={call}>Позвонить</Button>
            <Button style={{flex: 1}} onPress={write}>Написать</Button>
        </HStack>
    </View>


}
