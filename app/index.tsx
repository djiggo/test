import {View} from "react-native";
import {Link, Stack, Tabs} from "expo-router";
import {Button} from "native-base";


export default function HomePage() {
    return (
        <View style={{flex: 1, justifyContent: "center", alignItems: "center", rowGap: 24}}>
            <Stack.Screen options={{title: "Home page"}}/>
            <Link href="/vehicles" asChild>
                <Button>Список ТС</Button>
            </Link>
            <Link href="/settings" asChild>
                <Button>Настройки</Button>
            </Link>
        </View>
    );
}
