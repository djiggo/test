import {Stack} from "expo-router";
import {View, Text} from "react-native";
import {Select} from "native-base";
import {useState} from "react";

export default function SettingsPage() {


    // TODO This should be moved to Context or Store
    const [lang, setLang] = useState('ru');

    return <View style={{flex: 1, justifyContent: "center", rowGap: 6, padding: 24}}>
        <Stack.Screen options={{title: "Settings"}}/>

        <Text>Язык:</Text>
        <Select onValueChange={(v) => setLang(v)} selectedValue={lang}>
            <Select.Item label='Русский' value='ru'/>
            <Select.Item label='English' value='en'/>
        </Select>
    </View>

}