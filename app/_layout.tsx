import {Stack, Slot} from "expo-router";
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import React from "react";
import {NativeBaseProvider} from "native-base";

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {}
    }
});
export default function Layout() {
    return <QueryClientProvider client={queryClient}>
        <NativeBaseProvider>
            <Stack>
                <Stack.Screen name="index"/>
                <Stack.Screen name="vehicles"/>
            </Stack>
        </NativeBaseProvider>
    </QueryClientProvider>
}

