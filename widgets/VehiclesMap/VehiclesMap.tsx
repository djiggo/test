import MapView, {Marker} from "react-native-maps";
import {getMarkersBoundingBox} from "../../shared/lib/geo/getMarkersBoundingBox";
import {useVehiclesListQuery} from "../../entities/Vehicle/quieries/useVehiclesListQuery";
import {Ionicons} from '@expo/vector-icons';
import {VehicleType} from "../../entities/Vehicle/Vehicle";

export const VehiclesMap = () => {

    const {data: vehiclesList} = useVehiclesListQuery();
    const vehiclesCoords = vehiclesList.map((vehicle) => vehicle.position);

    const getVehicleIconName = (type: VehicleType) => {
        switch (type) {
            case VehicleType.Passenger:
                return 'man';
            case VehicleType.Cargo:
                return 'archive';
            case VehicleType.Special:
                return 'cart'

        }
    }

    return <MapView style={{width: "100%", height: "100%"}} initialRegion={getMarkersBoundingBox(vehiclesCoords, 0.02)}>
        {vehiclesList.map((vehicle, index) => (
            <Marker
                key={vehicle.id}
                coordinate={vehicle.position}
                title={`ТС #${vehicle.id}`}
            >
                <Ionicons name={getVehicleIconName(vehicle.type)} size={32} color="black" />
            </Marker>
        ))}
    </MapView>
}