import {Box, FlatList, Select} from "native-base";
import {useVehiclesListQuery} from "../../entities/Vehicle/quieries/useVehiclesListQuery";
import {VehicleListItem} from "../../shared/ui/VehicleListItem/VehicleListItem";
import {Vehicle, VehicleType} from "../../entities/Vehicle/Vehicle";
import {ListRenderItemInfo, View} from "react-native";
import {useState} from "react";
import {useRouter} from "expo-router";

export const VehiclesList = () => {

    const router = useRouter();

    const {data: vehicleList} = useVehiclesListQuery();
    const [selectedVehicleType, setSelectedVehicleType] = useState<VehicleType>();

    const vehicleListFiltered = !selectedVehicleType ? vehicleList : vehicleList.filter((vehicle) => vehicle.type === selectedVehicleType);

    const showVehicleInfo = (id: number) => {
        router.push("/vehicles/" + id);
    }

    function renderListItem(info: ListRenderItemInfo<Vehicle>) {

        return <VehicleListItem
            key={info.item.id}
            name={`ТС #${info.item.id}`}
            driverName={info.item.driver.name}
            type={info.item.type}
            onPress={() => showVehicleInfo(info.item.id)}
        />;
    }

    return <View>
        <Select
            selectedValue={selectedVehicleType}
            placeholder="Filter by type"
            onValueChange={(value: VehicleType) => setSelectedVehicleType(value)}
        >
            <Select.Item label="all" value={undefined}/>
            <Select.Item label={VehicleType.Cargo} value={VehicleType.Cargo}/>
            <Select.Item label={VehicleType.Passenger} value={VehicleType.Passenger}/>
            <Select.Item label={VehicleType.Special} value={VehicleType.Special}/>
        </Select>
        <Box padding={2}>
            <FlatList data={vehicleListFiltered} renderItem={renderListItem}/>
        </Box>
    </View>

}