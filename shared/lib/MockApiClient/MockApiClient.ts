import vehiclesJSON from '../../../vehicles.json';

export const MockApiClient = async (path: string) => {
    switch (true) {
        case /\/vehicles/.test(path):
            return vehiclesJSON;
        case /\/vehicle\/\d+$/.test(path):
            const id = +path.match(/\d+/)[0];
            return vehiclesJSON.find((v) => v.id === id);
    }

}