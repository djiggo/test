import {Region} from "react-native-maps/lib/sharedTypes";

type Coordinate = {
    latitude: number,
    longitude: number,
}
export const getMarkersBoundingBox = (coordinates: Coordinate[], offset: number = 0) => {

    if (coordinates.length === 0) {
        throw new Error("Coordinates array must not be empty.");
    }

    let minLat = Number.MAX_VALUE;
    let maxLat = -Number.MAX_VALUE;
    let minLon = Number.MAX_VALUE;
    let maxLon = -Number.MAX_VALUE;

    for (const coord of coordinates) {
        const lat = coord.latitude;
        const lon = coord.longitude;

        if (isNaN(lat) || isNaN(lon)) {
            throw new Error("Invalid coordinate format.");
        }

        minLat = Math.min(minLat, lat);
        maxLat = Math.max(maxLat, lat);
        minLon = Math.min(minLon, lon);
        maxLon = Math.max(maxLon, lon);
    }

    const centerLat = (minLat + maxLat) / 2;
    const centerLon = (minLon + maxLon) / 2;
    const latDelta = Math.abs(maxLat - minLat);
    const lonDelta = Math.abs(maxLon - minLon);

    return {
        latitude: centerLat,
        longitude: centerLon,
        latitudeDelta: latDelta + offset,
        longitudeDelta: lonDelta + offset,
    };

}