import {FC} from "react";
import {VehicleType} from "../../../entities/Vehicle/Vehicle";
import {Box, VStack, Text, HStack, Spacer, Pressable} from "native-base";


type Props = {
    name: string;
    driverName: string
    type: VehicleType
    onPress?: () => void
}
export const VehicleListItem: FC<Props> = ({name, driverName, type, onPress}) => {

    return <Pressable onPress={onPress}>
        <Box borderBottomWidth="1" borderColor="muted.300">
            <HStack space={[2, 3]} justifyContent="space-between">
                <VStack>
                    <Text color="warmGray.800" bold>{name}</Text>
                    <Text color="coolGray.800">{driverName}</Text>
                </VStack>
                <Spacer />
                <Text color="coolGray.800">{type}</Text>
            </HStack>
        </Box>
    </Pressable>
}